BEGIN;

CREATE SCHEMA faker;
CREATE EXTENSION faker SCHEMA faker CASCADE;

SELECT faker.faker('fr_FR');

SELECT faker.department() IS NOT NULL;

SELECT faker.military_apo() IS NULL;

SELECT faker.faker('en_US');
SELECT faker.military_apo() IS NOT NULL;

ROLLBACK;
