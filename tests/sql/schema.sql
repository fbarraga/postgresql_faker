BEGIN;


CREATE SCHEMA "pL0p";
CREATE EXTENSION faker SCHEMA "pL0p" CASCADE;

SELECT "pL0p".faker('fr_FR');

SELECT "pL0p".seed(4321);

SELECT "pL0p".name() = 'Antoine-Lucas Andre';

ROLLBACK;
