BEGIN;

CREATE SCHEMA faker;
CREATE EXTENSION faker SCHEMA faker CASCADE;

SELECT faker.faker('fr_FR');

SELECT faker.seed(4321);

SELECT faker.name() = 'Antoine-Lucas Andre';

SELECT faker.name() IS NOT NULL;

SELECT faker.seed(4321);

SELECT faker.name() = 'Antoine-Lucas Andre';


ROLLBACK;
