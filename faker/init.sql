
SET LOCAL search_path to @extschema@;

-- initialize the faker engine
-- usage: SELECT faker.faker(ARRAY('fr_FR','ja_JP'))
CREATE OR REPLACE FUNCTION faker(
  locales TEXT[]
)
RETURNS BOOLEAN
AS $$
  from faker import Faker
  GD['Faker'] = Faker(locales)
  return True
$$
  LANGUAGE plpython3u
;

--
-- initialize the faker engine with one or zero locale
-- usage:
--  - SELECT faker.faker()
--  - SELECT faker.faker('de_DE')
--
CREATE OR REPLACE FUNCTION faker(
  locale TEXT DEFAULT 'en_US'
)
RETURNS BOOLEAN
AS $$
  from faker import Faker
  GD['Faker'] = Faker(locale)
  return True
$$
  LANGUAGE plpython3u
;

CREATE OR REPLACE FUNCTION autoinit()
RETURNS BOOLEAN
AS $$
  test = """
    SELECT
      pg_catalog.current_setting('faker.autoinit', True)::BOOLEAN AS autoinit,
      pg_catalog.current_setting('faker.locales', True) AS locales,
      pg_catalog.current_setting('faker.seed', True) AS seed
    ;
  """
  settings = plpy.execute(test)
  if settings[0]['autoinit'] is not None and not settings[0]['autoinit'] :
    plpy.warning(
      "faker is not initialized.",
      hint="Use SELECT @extschema@.faker(); first or set faker.autoinit to True."
    )
    return False

  from faker import Faker
  if settings[0]['locales'] is None :
    GD['Faker'] = Faker()
  else:
    GD['Faker'] = Faker([l.strip() for l in settings[0]['locales'].split(',')])

  if settings[0]['seed'] is not None:
    GD['Faker'].seed_instance(settings[0]['seed'])

  return True
$$
  LANGUAGE plpython3u
;


--
-- Seeding the generator
-- https://github.com/joke2k/faker#seeding-the-generator
--
CREATE OR REPLACE FUNCTION seed(
  seed TEXT
)
RETURNS BOOLEAN
AS $$
  try:
    GD['Faker'].seed_instance(seed)
  except KeyError:
    plpy.warning(
      "faker is not initialized.",
      hint='Use SELECT @extschema@.faker(); first.'
    )
    return False
  return True
$$
  LANGUAGE plpython3u
;

CREATE OR REPLACE FUNCTION seed(
  seed INTEGER
)
RETURNS BOOLEAN
AS $$
  try:
    GD['Faker'].seed_instance(seed)
  except KeyError:
    plpy.warning(
      "faker is not initialized.",
      hint='Use SELECT @extschema@.faker(); first.'
    )
    return False
  return True
$$
  LANGUAGE plpython3u
;


