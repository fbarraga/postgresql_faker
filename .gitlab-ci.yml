## PRO TIP : Test your modifications locally with:
## $ gitlab-ci-multi-runner exec docker {name_of_the_job}



variables:
    PGDATA: /var/lib/postgresql/data
    PGUSER: postgres
    EXTDIR: /usr/share/postgresql/13/extension/
    PSQL: psql -v ON_ERROR_STOP=1
    POSTGRES_DB: nice_marmot
    POSTGRES_USER: runner
    POSTGRES_PASSWORD: plop
    SAST_EXCLUDED_PATHS: '_venv'

stages:
  - lint
  - build
  - regress
  - test
  - deploy

# Default version 
image: postgres:14

# We need to launch manually the instance because the entrypoint is skipped.
.init_PG_instance: &init_PG_instance
  before_script:
    - mkdir -p $PGDATA
    - mkdir -p $EXTDIR
    - chown postgres $PGDATA
    - gosu postgres initdb
    - gosu postgres pg_ctl start


##
## L I N T
##

lint-python:
  stage: lint
  needs: []
  image: python:3.7-slim
  script:
    - pip3 install -r requirements.txt
    - python3 -m flake8 faker

lint-markdown:
  stage: lint
  needs: []
  image: ruby:alpine
  script:
    - gem install mdl
    - mdl *.md

##
## B U I L D
##

make:
  stage: build
  needs: []
  image: centos:7
  script:
    # http://blog.cloud-mes.com/2020/05/16/fix-yum-update-postgresql12-to-v12-dot-3-require-llvm-toolset-7-clang-equals-4-dot-0-1-dependency-problem/
    - yum -y install centos-release-scl-rh epel-release
    - yum -y install llvm-toolset-7-clang
    # Install PGDG repo
    - yum -y install https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm
    # build tools
    - yum -y install git make gcc zip postgresql14 postgresql14-server postgresql14-devel postgresql14-contrib python3-pip
    # python packages
    - python3 -m pip install --no-cache-dir -r requirements-ci.txt
    - export PATH="$PATH:/usr/pgsql-14/bin"
    # build
    - make extension
    - make pgxn
  artifacts:
    untracked: true
    expire_in: 1 day

# /!\ does not work, we need to install Python3.6
make-debian-bullseye:
  stage: build
  needs: []
  image: postgres:14
  <<: *init_PG_instance
  script:
    - apt-get update && apt-get install -y make zip postgresql-plpython3-14 postgresql-server-dev-14 python3-pip python3-venv python3-virtualenv
    - pip3 install -r requirements.txt
    - make extension
    - make pgxn
    - make install
    - make installcheck || diff results tests/expected
  artifacts:
    untracked: true
#    expire_in: 1 day
  when: manual

# install from source on fedora
# /!\ does not work, we need to install Python3.6
make_fedora34:
  stage: build
  image: fedora:34
  script:
    - dnf -y install https://download.postgresql.org/pub/repos/yum/reporpms/F-34-x86_64/pgdg-fedora-repo-latest.noarch.rpm
    - dnf -qy module disable postgresql
    - dnf install --assumeyes postgresql13-contrib postgresql13-devel libpq-devel postgresql13-plpython3 python3-faker python3-dateutil make git redhat-rpm-config clang ccache python3-virtualenv
    - export PATH=$PATH:/usr/pgsql-13/bin/
    - make clean extension install
    - export PGDATA=/var/lib/postgresql/data
    - mkdir -p $PGDATA
    - chown postgres $PGDATA
    - su postgres -c initdb
    - su postgres -c 'pg_ctl start'
    - export PGUSER=postgres
    - make installcheck || diff results tests/expected
  when: manual

##
## R E G R E S S
##

## Note: we can't use the old docker images based on debian stretch
## because of a python/pip depency problems

PG9-alpine:
  stage: regress
  needs: ["make"]
  image: postgres:9.6-alpine
  <<: *init_PG_instance
  script:
    - apk add --no-cache postgresql-plpython3
    - apt-get update && apt-get install -y make postgresql-plpython3-9.6 postgresql-server-dev-9.6 python3-pip python3-virtualenv
    - pip3 install --upgrade pip
    - pip3 install -r requirements.txt
    - make install
    - make installcheck || diff results tests/expected
  artifacts:
    untracked: true
    expire_in: 1 day
  when: manual

PG9-buster:
  stage: regress
  needs: ["make"]
  image: postgres:9.6-buster
  <<: *init_PG_instance
  script:
    - apt-get update && apt-get install -y make postgresql-plpython3-9.6 postgresql-server-dev-9.6 python3-pip python3-virtualenv
    - pip3 install -r requirements.txt
    - make install
    - make installcheck || diff results tests/expected
  artifacts:
    untracked: true
    expire_in: 1 day

PG10-buster:
  stage: regress
  needs: ["make"]
  image: postgres:10-buster
  <<: *init_PG_instance
  script:
    - apt-get update && apt-get install -y make postgresql-plpython3-10 postgresql-server-dev-10 python3-pip python3-virtualenv
    - pip3 install -r requirements.txt
    - make install
    - make installcheck || diff results tests/expected
  artifacts:
    untracked: true
    expire_in: 1 day


PG11-buster:
  stage: regress
  needs: ["make"]
  image: postgres:11-buster
  <<: *init_PG_instance
  script:
    - apt-get update && apt-get install -y make postgresql-plpython3-11 postgresql-server-dev-11 python3-pip python3-virtualenv
    - pip3 install -r requirements.txt
    - make install
    - make installcheck || diff results tests/expected
  artifacts:
    untracked: true
    expire_in: 1 day


PG12-bullseye:
  stage: regress
  needs: ["make"]
  image: postgres:12
  <<: *init_PG_instance
  script:
    - apt-get update && apt-get install -y make postgresql-plpython3-12 git postgresql-server-dev-12 python3-pip
    - pip3 install -r requirements.txt
    - make install
    - make installcheck || diff results tests/expected
  artifacts:
    untracked: true
    expire_in: 1 day

PG14-bullseye:
  stage: regress
  needs: ["make"]
  image: postgres:14
  <<: *init_PG_instance
  script:
    - apt-get update && apt-get install -y make postgresql-plpython3-14 postgresql-server-dev-14 python3-pip
    - pip3 install -r requirements.txt
    - make install
    - make installcheck || diff results tests/expected
  artifacts:
    untracked: true
    expire_in: 1 day

PG11-rocky8:
  stage: regress
  needs: ["make"]
  image: rockylinux/rockylinux:8
  script:
    - dnf -y install https://download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm
    - dnf -qy module disable postgresql
    - dnf -y install make redhat-rpm-config clang diffutils postgresql11-plpython3 postgresql11 postgresql11-server postgresql11-devel python3-virtualenv
    - dnf -y --nogpgcheck install python3-faker python3-dateutil
    # python3-faker requires python3-text-unidecode in EPEL
    - dnf -y install epel-release
    - dnf -y install python3-text-unidecode
    - export PATH="$PATH:/usr/pgsql-11/bin"
    - mkdir -p $PGDATA
    - chown postgres:postgres $PGDATA
    - su postgres -c "initdb $PGDATA"
    - su postgres -c "pg_ctl start"
    - make install
    - make installcheck || diff results tests/expected
  artifacts:
    untracked: true
    expire_in: 1 day

# install from source on fedora
PG13_fedora34:
  stage: regress
  image: fedora:34
  script:
    - dnf -y install https://download.postgresql.org/pub/repos/yum/reporpms/F-34-x86_64/pgdg-fedora-repo-latest.noarch.rpm
    - dnf -qy module disable postgresql
    - dnf install --assumeyes postgresql13-contrib postgresql13-devel libpq-devel postgresql13-plpython3 python3-faker python3-dateutil make git redhat-rpm-config clang ccache python3-virtualenv
    - export PATH=$PATH:/usr/pgsql-13/bin/
    - export PGDATA=/var/lib/postgresql/data
    - mkdir -p $PGDATA
    - chown postgres $PGDATA
    - su postgres -c initdb
    - su postgres -c 'pg_ctl start'
    - export PGUSER=postgres
    - make install
    - make installcheck || diff results tests/expected




##
## T E S T
##

include:
  - template: Security/SAST.gitlab-ci.yml

install_pgxn:
  stage: test
  image: postgres:14
  needs: []
  script:
    - apt-get update && apt-get install -y make gcc postgresql-plpython3-14 postgresql-server-dev-14 pgxnclient
    - pgxn install postgresql_faker
  when: manual


install_yum_centos8_pg13:
  stage: test
  image: centos:8
  script:
    - dnf -y install https://download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm
    - dnf -qy module disable postgresql                                                                                                                  -
    - dnf install postgresql13-contrib postgresql_faker_13
    - mkdir -p $PGDATA
    - chown postgres $PGDATA
    - su postgres -c /usr/pgsql-13/bin/initdb $PGDATA
    - su postgres -c "/usr/pgsql-13/bin/pg_ctl start"
    - su postgres -c "psql -c 'CREATE EXTENSION faker CASCADE;'"
  when: manual


##
## D E P L O Y
##

docker_latest:
  stage: deploy
  image: docker:19.03.8
  services:
    - docker:19.03.8-dind
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $CI_REGISTRY/dalibo/postgresql_faker:latest . --file docker/Dockerfile
    - docker push $CI_REGISTRY/dalibo/postgresql_faker:latest
  only:
    - master@dalibo/postgresql_faker

docker_stable:
  stage: deploy
  image: docker:19.03.8
  services:
    - docker:19.03.8-dind
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $CI_REGISTRY/dalibo/postgresql_faker:stable . --file docker/Dockerfile
    - docker push $CI_REGISTRY/dalibo/postgresql_faker:stable
  only:
    - stable@dalibo/postgresql_faker



